### MAIN ORIENTATION ###

Informazioni sull'orientamento di un'immagine possono esser derivate dai momenti centrali di secondo ordine a partire dalla matrice di covarianza. 

Avendo definito tali momenti


\mu'_{20} = \mu_{20} / \mu_{00} = M_{20}/M_{00} - \bar{x}^2
\mu'_{02} = \mu_{02} / \mu_{00} = M_{02}/M_{00} - \bar{y}^2
\mu'_{11} = \mu_{11} / \mu_{00} = M_{11}/M_{00} - \bar{x}\bar{y}


la matrice di covarianza dell'immagine I(x,y) è

\operatorname{cov}[I(x,y)] = \begin{bmatrix} \mu'_{20}  & \mu'_{11} \\ \mu'_{11} & \mu'_{02} \end{bmatrix}.

Gli autovettori di tale matrice corrispondono all'asse maggiore ed all'asse minore dell'ellisse associato all'intensità dell'immagine: 
l'orientazione dell'immagine può allora essere estratta dall'angolo che il semiasse maggiore (autovettore associato all'autovalore massimo) forma con la direzione orizzontale. 
Tale angolo Θ è dato dalla seguente formula:

\Theta = \frac{1}{2} \arctan \left( \frac{2\mu'_{11}}{\mu'_{20} - \mu'_{02}} \right)

obiettivo: data una immagine, definire una funzione che restituisca la stessa immagine con orientazione principale parallela all'asse verticale.

rif: http://it.wikipedia.org/wiki/Momento_(elaborazione_delle_immagini)

